from flask import Flask


app = Flask(__name__)
app.debug = True

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:@localhost/my_bookmark'
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
app.config['USERNAME'] = 'admin'
app.config['PASSWORD'] = 'admin'
